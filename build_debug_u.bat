@echo off
title Rusting mtl Debug...
color 07
SET RUSTFLAGS=-Awarnings
cargo build
ren "target\debug\mtl.dll" "mtl-debug.qdp"
move "target\debug\mtl-debug.qdp" "."
copy "mtl-debug.qdp" "D:\SCBW 1.16.1\maps\BroodWar\[Cosmonarchy]\plugins\mtl.qdp"
pause > nul