@echo off
title Rusting mtl Release...
color 07
SET RUSTFLAGS=-Awarnings
cargo build --release
ren "target\release\mtl.dll" "mtl-release.qdp"
move "target\release\mtl-release.qdp" "."
copy "mtl-release.qdp" "D:\SCBW 1.16.1\maps\BroodWar\[Cosmonarchy]\plugins\mtl.qdp"
pause > nul