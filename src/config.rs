use std::collections::BTreeMap;
use std::sync::{Arc, Mutex};

use smallvec::SmallVec;

use bw_dat::{UnitId, OrderId};
use bw_dat::expr::{IntExpr, BoolExpr};
use failure::{Context, Error, ResultExt};
use crate::ini::Ini;
use crate::upgrades::{Upgrades, Upgrade, UpgradeChanges, State, Stat};

/// Various timers, in frames, unlike bw's 8-frame chunks.
/// Though the 8-frameness for updates causes inaccuracy anyways.
#[derive(Default)]
pub struct Timers {
    pub hallucination_death: Option<u32>,
    pub unit_deaths: Vec<(UnitId, u32)>,
    pub matrix: Option<u32>,
    pub stim: Option<u32>,
    pub ensnare: Option<u32>,
    pub lockdown: Option<u32>,
    pub irradiate: Option<u32>,
    pub stasis: Option<u32>,
    pub plague: Option<u32>,
    pub maelstrom: Option<u32>,
    pub acid_spores: Option<u32>,
}

#[derive(Default)]
pub struct Supplies {
    pub zerg_max: Option<u32>,
    pub terran_max: Option<u32>,
    pub protoss_max: Option<u32>,
}

pub struct Config {
    pub timers: Timers,
    pub supplies: Supplies,
    pub upgrades: Upgrades,
    pub return_cargo_softcode: bool,
    pub zerg_building_training: bool,
}

impl Config {
    pub fn requires_order_hook(&self) -> bool {
        let Config {
            timers: _,
            supplies: _,
            return_cargo_softcode,
            zerg_building_training: _,
            ref upgrades,
        } = *self;
        return_cargo_softcode || !upgrades.upgrades.is_empty()
    }

    pub fn requires_secondary_order_hook(&self) -> bool {
        let Config {
            timers: _,
            supplies: _,
            return_cargo_softcode: _,
            zerg_building_training,
            ref upgrades,
        } = *self;
        zerg_building_training || !upgrades.upgrades.is_empty()
    }
}

lazy_static! {
    static ref CONFIG: Mutex<Option<Arc<Config>>> = Mutex::new(None);
}

fn bool_field(out: &mut bool, value: &str, field: &'static str) -> Result<(), Error> {
    match value {
        "true" | "True" | "1" | "y" | "Y" => *out = true,
        "false" | "False" | "0" | "n" | "N" => *out = true,
        _ => {
            let msg = format!("Invalid value `{}` for bool {}", value, field);
            return Err(Context::new(msg).into());
        }
    }
    Ok(())
}

fn u32_field(out: &mut Option<u32>, value: &str, field_name: &'static str) -> Result<(), Error> {
    let value = parse_u32(value)
        .map_err(|e| e.context(format!("{} is not u32", field_name)))?;
    *out = Some(value);
    Ok(())
}

fn parse_u32(value: &str) -> Result<u32, Error> {
    Ok(if value.starts_with("0x") {
        u32::from_str_radix(&value[2..], 16)?
    } else {
        u32::from_str_radix(value, 10)?
    })
}

fn parse_u16(value: &str) -> Result<u16, Error> {
    Ok(if value.starts_with("0x") {
        u16::from_str_radix(&value[2..], 16)?
    } else {
        u16::from_str_radix(value, 10)?
    })
}

fn parse_u8(value: &str) -> Result<u8, Error> {
    Ok(if value.starts_with("0x") {
        u8::from_str_radix(&value[2..], 16)?
    } else {
        u8::from_str_radix(value, 10)?
    })
}

fn parse_u8_list<'a>(values: &'a str) -> impl Iterator<Item=Result<u8, Error>> + 'a {
    values.split(",").map(|x| parse_u8(x.trim()))
}

fn parse_state(value: &str) -> Result<State, Error> {
    Ok(match value {
        "self_cloaked" => State::SelfCloaked,
        "arbiter_cloaked" => State::ArbiterCloaked,
        "burrowed" => State::Burrowed,
        "incomplete" => State::Incomplete,
        "disabled" => State::Disabled,
        "damaged" => State::Damaged,
        _ => {
            fn in_braces<'a>(text: &'a str, prefix: &str) -> Result<Option<&'a str>, Error> {
                if text.starts_with(prefix) {
                    let text = &text[prefix.len()..].trim();
                    let ok = text.get(..1) == Some("(") &&
                        text.get(text.len() - 1..) == Some(")");
                    if !ok {
                        Err(format_err!("Invalid syntax"))
                    } else {
                        Ok(Some(&text[1..text.len() - 1]))
                    }
                } else {
                    Ok(None)
                }
            }
            if let Some(text) = in_braces(value, "order")? {
                let orders = parse_u8_list(text).map(|x| x.map(|x| OrderId(x)))
                    .collect::<Result<_, Error>>()?;
                State::Order(orders)
            } else if let Some(text) = in_braces(value, "animation")? {
                let anims = parse_u8_list(text).collect::<Result<_, Error>>()?;
                State::IscriptAnim(anims)
            } else {
                return Err(format_err!("Unknown state {}", value));
            }
        }
    })
}

fn parse_stat(key: &str) -> Result<Stat, Error> {
    Ok(match key {
        "hp_regen" => Stat::HpRegen,
        "shield_regen" => Stat::ShieldRegen,
        "energy_regen" => Stat::EnergyRegen,
        "cooldown" => Stat::Cooldown,
        "larva_timer" => Stat::LarvaTimer,
        "mineral_harvest_time" => Stat::MineralHarvestTime,
        "gas_harvest_time" => Stat::GasHarvestTime,
        "unload_cooldown" => Stat::UnloadCooldown,
        "creep_spread_timer" => Stat::CreepSpreadTimer,
        "mineral_harvest_reduce" => Stat::MineralHarvestReduce,
        "mineral_harvest_carry" => Stat::MineralHarvestCarry,
        "gas_harvest_reduce" => Stat::GasHarvestReduce,
        "gas_harvest_carry" => Stat::GasHarvestCarry,
        "gas_harvest_carry_depleted" => Stat::GasHarvestCarryDepleted,
        "set_unit_id" => Stat::SetUnitId,
        "player_color" => Stat::PlayerColor,
        "player_color_palette" => Stat::PlayerColorPalette,
        "show_energy" => Stat::ShowEnergy,
        "show_shields" => Stat::ShowShields,
        _ => return Err(format_err!("Unknown stat {}", key)),
    })
}

/// NOTE: Passing an empty string will not yield anything
fn brace_aware_split<'a>(text: &'a str, tok: &'a str) -> BraceSplit<'a> {
    BraceSplit(text, tok)
}

struct BraceSplit<'a>(&'a str, &'a str);

impl<'a> Iterator for BraceSplit<'a> {
    type Item = &'a str;
    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_empty() {
            return None;
        }
        let bytes = self.0.as_bytes();
        let mut depth = 0;
        for (i, &b) in bytes.iter().enumerate() {
            if b == b'(' {
                depth += 1;
            } else if b == b')' {
                depth -= 1;
            } else if depth == 0 {
                if let Some(x) = self.0.get(i..) {
                    if x.starts_with(self.1) {
                        let result = &self.0[..i];
                        self.0 = &self.0[i + self.1.len()..];
                        return Some(result);
                    }
                }
            }
        }
        let result = &self.0[..];
        self.0 = "";
        Some(result)
    }
}

fn parse_upgrade_condition(condition: &str) -> Result<BoolExpr, Error> {
    BoolExpr::parse(condition.as_bytes())
        .map_err(|e| e.into())
}

fn parse_int_expr(expr: &str) -> Result<IntExpr, Error> {
    IntExpr::parse(expr.as_bytes())
        .map_err(|e| e.into())
}

fn parse_int_expr_tuple(expr: &str, count: u8) -> Result<Vec<IntExpr>, Error> {
    let expr = expr.trim();
    if !expr.starts_with("(") || !expr.ends_with(")") {
        return Err(format_err!("Expected braced list"));
    }
    let expr = &expr[1..expr.len() - 1];
    let result = brace_aware_split(expr, ",")
        .map(|x| x.trim())
        .map(|x| parse_int_expr(x))
        .collect::<Result<Vec<_>, Error>>()?;
    if result.len() != count as usize {
        return Err(format_err!("Expected {} items, got {}", count, result.len()));
    }
    Ok(result)
}

pub fn read_config(mut data: &[u8]) -> Result<Config, Error> {
    let error_invalid_field = |name: &'static str| {
        format_err!("Invalid field {}", name)
    };

    let ini = Ini::open(&mut data)
        .map_err(|e| e.context("Unable to read ini"))?;
    let mut timers: Timers = Default::default();
    let mut return_cargo_softcode = false;
    let mut zerg_building_training = false;
    let mut supplies: Supplies = Default::default();
    let mut upgrades: BTreeMap<u8, BTreeMap<Vec<State>, Vec<UpgradeChanges>>> = BTreeMap::new();
    for section in &ini.sections {
        let name = &section.name;
        if name == "timers" {
            for &(ref key, ref val) in &section.values {
                match &**key {
                    "hallucination_death" => {
                        u32_field(&mut timers.hallucination_death, &val, "hallucination_death")?
                    }
                    "matrix" => u32_field(&mut timers.matrix, &val, "matrix")?,
                    "stim" => u32_field(&mut timers.stim, &val, "stim")?,
                    "ensnare" => u32_field(&mut timers.ensnare, &val, "ensnare")?,
                    "lockdown" => u32_field(&mut timers.lockdown, &val, "lockdown")?,
                    "irradiate" => u32_field(&mut timers.irradiate, &val, "irradiate")?,
                    "stasis" => u32_field(&mut timers.stasis, &val, "stasis")?,
                    "plague" => u32_field(&mut timers.plague, &val, "plague")?,
                    "maelstrom" => u32_field(&mut timers.maelstrom, &val, "maelstrom")?,
                    "acid_spores" => u32_field(&mut timers.acid_spores, &val, "acid_spores")?,
                    "unit_deaths" => {
                        for pair in val.split(",") {
                            let mut tokens = pair.split(":");
                            let unit_id = tokens.next()
                                .ok_or_else(|| error_invalid_field("timers.unit_deaths"))?;
                            let unit_id = parse_u16(unit_id.trim()).map_err(|e| {
                                let timer_index = timers.unit_deaths.len();
                                e.context(format!(
                                    "timers.unit_deaths #{} unit id is not u16",
                                    timer_index
                                ))
                            })?;
                            let time = tokens.next()
                                .ok_or_else(|| error_invalid_field("timers.unit_deaths"))?;
                            let time = parse_u32(time.trim()).map_err(|e| {
                                let timer_index = timers.unit_deaths.len();
                                e.context(format!(
                                    "timers.unit_deaths #{} time is not u32",
                                    timer_index
                                ))
                            })?;
                            timers.unit_deaths.push((UnitId(unit_id), time));
                        }
                    }
                    x => return Err(Context::new(format!("unknown key timers.{}", x)).into()),
                }
            }
        } else if name == "supplies" {
            for &(ref key, ref val) in &section.values {
                match &**key {
                    "zerg_max" => u32_field(&mut supplies.zerg_max, &val, "zerg_max")?,
                    "terran_max" => u32_field(&mut supplies.terran_max, &val, "terran_max")?,
                    "protoss_max" => u32_field(&mut supplies.protoss_max, &val, "protoss_max")?,
                    x => return Err(Context::new(format!("unknown key supplies.{}", x)).into()),
                }
            }
        } else if name == "orders" {
            for &(ref key, ref val) in &section.values {
                match &**key {
                    "return_cargo_softcode" => {
                        bool_field(&mut return_cargo_softcode, &val, "return_cargo_softcode")?
                    }
                    "zerg_training" => {
                        bool_field(&mut zerg_building_training, &val, "zerg_training")?
                    }
                    x => return Err(Context::new(format!("unknown key {}", x)).into()),
                }
            }
        } else if name.starts_with("upgrade") {
            let mut tokens = name.split(".").skip(1);
            let generic_error = || {
                format_err!(
                    "Upgrade section {} isn't formatted as \"upgrade.<x>.level.<y>\"", name
                )
            };
            let id = tokens.next().ok_or_else(generic_error)?;
            let level_str = tokens.next().ok_or_else(generic_error)?;
            let level = tokens.next().ok_or_else(generic_error)?;
            if level_str != "level" {
                return Err(generic_error());
            }
            let id = parse_u8(id)
                .map_err(|e| e.context(format!("Invalid upgrade id \"{}\"", id)))?;
            let level = parse_u8(level)
                .map_err(|e| e.context(format!("Invalid upgrade level \"{}\"", level)))?;

            let mut units = Vec::new();
            let mut states = Vec::new();
            let mut stats = Vec::new();
            let mut condition = None;
            for &(ref key, ref val) in &section.values {
                match &**key {
                    "units" => {
                        for tok in val.split(",").map(|x| x.trim()) {
                            let id = parse_u16(tok)
                                .map_err(|e| e.context(format!("{} units", name)))?;
                            units.push(UnitId(id));
                        }
                    }
                    "state" => {
                        for tok in brace_aware_split(val, ",").map(|x| x.trim()) {
                            let state = parse_state(tok)
                                .map_err(|e| e.context(format!("{} state", name)))?;
                            states.push(state);
                        }
                    }
                    "condition" => {
                        if condition.is_some() {
                            return Err(format_err!("Cannot have multiple conditions"));
                        }
                        let cond = parse_upgrade_condition(val)
                            .with_context(|_| format!("Condition of {}", name))?;
                        condition = Some(cond);
                    }
                    x => {
                        let stat = parse_stat(x)
                            .map_err(|e| e.context(format!("In {}:{}", name, x)))?;
                        let value_count = stat.value_count();
                        let values = if value_count == 1 {
                            let mut vec = SmallVec::new();
                            let val = parse_int_expr(&val)
                                .map_err(|e| e.context(format!("In {}:{}", name, x)))?;
                            vec.push(val);
                            vec
                        } else {
                            parse_int_expr_tuple(&val, value_count)
                                .map_err(|e| e.context(format!("In {}:{}", name, x)))?
                                .into()
                        };
                        stats.push((stat, values));
                    }
                }
            }
            if units.is_empty() {
                return Err(format_err!("{} did not specify any units", name));
            }
            let changes = UpgradeChanges {
                units,
                level,
                changes: stats,
                condition,
            };
            upgrades.entry(id).or_insert_with(|| Default::default())
                .entry(states).or_insert_with(|| Default::default())
                .push(changes);
        } else {
            return Err(format_err!("Unknown section name \"{}\"", name));
        }
    }
    let upgrades = upgrades.into_iter().map(|(id, mut changes)| {
        let mut all_matching_units = Vec::with_capacity(8);
        for change_lists in changes.values_mut() {
            for changes in change_lists.iter() {
                for &u in &changes.units {
                    if !all_matching_units.iter().any(|&x| x == u) {
                        all_matching_units.push(u);
                    }
                }
            }
            change_lists.sort_by_key(|x| x.level);
        }
        let upgrade = Upgrade {
            all_matching_units,
            changes,
        };
        (id as usize, upgrade)
    }).collect();
    let upgrades = Upgrades::new(upgrades);
    Ok(Config {
        timers,
        supplies,
        return_cargo_softcode,
        zerg_building_training,
        upgrades,
    })
}

pub fn set_config(config: Config) {
    *CONFIG.lock().unwrap() = Some(Arc::new(config));
}

pub fn config() -> Arc<Config> {
    CONFIG.lock().unwrap().as_ref().unwrap().clone()
}

#[test]
fn test_parse_states() {
    assert_eq!(parse_state("disabled").unwrap(), State::Disabled);
    assert_eq!(
        parse_state("order(4, 5, 0x40)").unwrap(),
        State::Order(vec![OrderId(4), OrderId(5), OrderId(0x40)].into())
    );
    assert_eq!(parse_state("animation(4, 0x5)").unwrap(), State::IscriptAnim(vec![4, 5].into()));
}

#[test]
fn test_brace_split() {
    let mut split = brace_aware_split("a, b, c", ",");
    assert_eq!(split.next().unwrap(), "a");
    assert_eq!(split.next().unwrap(), " b");
    assert_eq!(split.next().unwrap(), " c");
    assert_eq!(split.next(), None);

    let mut split = brace_aware_split(",a, b, c,", ",");
    assert_eq!(split.next().unwrap(), "");
    assert_eq!(split.next().unwrap(), "a");
    assert_eq!(split.next().unwrap(), " b");
    assert_eq!(split.next().unwrap(), " c");
    assert_eq!(split.next(), None);

    let mut split = brace_aware_split(" a s d ", ",");
    assert_eq!(split.next().unwrap(), " a s d ");
    assert_eq!(split.next(), None);

    let mut split = brace_aware_split("asd, (1, 2, 3), 45", ",");
    assert_eq!(split.next().unwrap(), "asd");
    assert_eq!(split.next().unwrap(), " (1, 2, 3)");
    assert_eq!(split.next().unwrap(), " 45");
    assert_eq!(split.next(), None);

    let mut split = brace_aware_split("asd, (1, 2, 3, 45", ",");
    assert_eq!(split.next().unwrap(), "asd");
    assert_eq!(split.next().unwrap(), " (1, 2, 3, 45");
    assert_eq!(split.next(), None);
}
