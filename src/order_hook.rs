use libc::c_void;

use bw_dat::unit as unit_id;
use bw_dat::{Unit, UpgradeId, upgrade};
use crate::bw;
use crate::config::config;
use crate::upgrades;
use crate::unit::UnitExt;

struct MiningOverride {
    old_amount: u16,
    carry: u8,
    resource: Unit,
}

pub unsafe extern fn order_hook(u: *mut c_void, orig: unsafe extern fn(*mut c_void)) {
    use bw_dat::order::*;

    let config = config();
    let game = crate::game::get();
    let unit = Unit::from_ptr(u as *mut bw::Unit).unwrap();
    let order = unit.order();
    let mut return_cargo_order = false;
    let mut return_cargo_softcoded = false;
    let mut harvest_gas_check = false;
    let mut harvest_minerals_check = false;
    let mut unload_check = false;
    let mut mining_override = None;
    let mut upgrade_check = None;
    let ground_cooldown_check = (**unit).ground_cooldown == 0;
    let air_cooldown_check = (**unit).air_cooldown == 0;
    match order {
        HARVEST_GAS => {
            return_cargo_order = true;
            harvest_gas_check = unit.order_state() == 0;
            if unit.order_state() == 5 && (**unit).order_timer == 0 {
                if let Some(target) = unit.target() {
                    let reduce = upgrades::gas_harvest_reduce(&config, game, unit);
                    let carry = upgrades::gas_harvest_carry(&config, game, unit);
                    let depleted = upgrades::gas_harvest_carry_depleted(&config, game, unit);
                    if reduce.is_some() || carry.is_some() || depleted.is_some() {
                        let reduce = u16::from(reduce.unwrap_or(8));
                        let carry = carry.unwrap_or(8);
                        let depleted = depleted.unwrap_or(2);
                        let current_resources = target.resource_amount();
                        // Can't mine less than 8 gas at once without it being depleted mining.
                        let (amount, carry) = if current_resources < 8 {
                            (0, depleted)
                        } else if current_resources <= reduce {
                            // Depletion message is only shown once remaining amount is
                            // between 0 and 7.
                            (8, depleted)
                        } else {
                            (current_resources - reduce + 8, carry)
                        };
                        target.set_resource_amount(amount);
                        mining_override = Some(MiningOverride {
                            old_amount: current_resources,
                            carry,
                            resource: target,
                        });
                    }
                }
            }
        }
        HARVEST_MINERALS => {
            harvest_minerals_check = unit.order_state() == 0 || unit.order_state() == 4;
            if unit.order_state() == 5 && (**unit).order_timer == 0 {
                if let Some(target) = unit.target() {
                    let reduce = upgrades::mineral_harvest_reduce(&config, game, unit);
                    let carry = upgrades::mineral_harvest_carry(&config, game, unit);
                    if reduce.is_some() || carry.is_some() {
                        let reduce = u16::from(reduce.unwrap_or(8));
                        let carry = carry.unwrap_or(8);
                        let current_resources = target.resource_amount();
                        let (amount, carry) = if current_resources <= reduce {
                            (1, current_resources as u8)
                        } else {
                            (current_resources - reduce + 8, carry)
                        };
                        target.set_resource_amount(amount);
                        mining_override = Some(MiningOverride {
                            old_amount: current_resources,
                            carry,
                            resource: target,
                        });
                    }
                }
            }
        }
        RETURN_MINERALS | RETURN_GAS | RESET_COLLISION_HARVESTER => {
            return_cargo_order = true;
        }
        UNLOAD | MOVE_UNLOAD => {
            unload_check = (**unit).order_timer == 0;
        }
        UPGRADE => {
	    if u16::from((**unit).unit_specific[9]) < 61 {//extender check
	 	    let upgrade = UpgradeId(u16::from((**unit).unit_specific[9]));
	            let level = (**unit).unit_specific[0xd];
	            if upgrade != upgrade::NONE {
	                if game.upgrade_level(unit.player(), upgrade) < level {
	                    upgrade_check = Some((upgrade, level));
	                }
	            }
	    }
          
        }
        _ => (),
    }
    if return_cargo_order {
        if config.return_cargo_softcode {
            return_cargo_softcoded = true;
            let player = unit.player() as usize;
            // BW only searches for resource depots if the player owns at least one of the
            // five hardcoded depots.
            // Harvest gas searches for the depots when deciding where the worker should
            // be spawned when exiting the gas mine.
            // The reset collision order is a highprio order, so it'll be able to
            // execute return cargo order immediately afterwards.
            (**game).completed_units_count[unit_id::COMMAND_CENTER.0 as usize][player] += 1;
        }
    }
    orig(u);
    if return_cargo_softcoded {
        let player = unit.player() as usize;
        (**game).completed_units_count[unit_id::COMMAND_CENTER.0 as usize][player] -= 1;
    }
    if harvest_gas_check {
        if unit.order_state() == 5 {
            if let Some(new_timer) = upgrades::gas_harvest_time(&config, game, unit) {
                (**unit).order_timer = new_timer;
            }
        }
    }
    if harvest_minerals_check {
        if unit.order_state() == 5 {
            if let Some(new_timer) = upgrades::mineral_harvest_time(&config, game, unit) {
                (**unit).order_timer = new_timer;
            }
        }
    }
    if unload_check {
        if (**unit).order_timer > 0 {
            if let Some(new_time) = upgrades::unload_cooldown(&config, game, unit) {
                (**unit).order_timer = new_time;
            }
        }
    }
    if ground_cooldown_check {
        if (**unit).ground_cooldown > 0 {
            if let Some(new_time) = upgrades::cooldown(&config, game, unit) {
                (**unit).ground_cooldown = new_time;
            }
        }
    }
    if air_cooldown_check {
        if (**unit).air_cooldown > 0 {
            if let Some(new_time) = upgrades::cooldown(&config, game, unit) {
                (**unit).air_cooldown = new_time;
            }
        }
    }
    if let Some(vars) = mining_override {
        if unit.target() == Some(vars.resource) {
            vars.resource.set_resource_amount(vars.old_amount);
        } else {
            (**unit).unit_specific[0xf] = vars.carry;
        }
    }
    if let Some((upgrade, level)) = upgrade_check {
		
        if game.upgrade_level(unit.player(), upgrade) == level {
            let mut upgrades = upgrades::global_state_changes();
            upgrades.upgrade_gained(&config, game, unit.player(), upgrade, level);
        }
    }
}

pub unsafe extern fn hidden_order_hook(u: *mut c_void, orig: unsafe extern fn(*mut c_void)) {
    order_hook(u, orig);
}

pub unsafe extern fn secondary_order_hook(u: *mut c_void, orig: unsafe extern fn(*mut c_void)) {
    use bw_dat::order::*;

    let unit = Unit::from_ptr(u as *mut bw::Unit).unwrap();
    let config = config();
    let mut spread_creep_check = false;
    let mut larva_spawn_check = false;
    let mut creep_spread_time = None;
    let mut larva_spawn_time = None;
    let mut zerg_group_flags = None;
    match unit.secondary_order() {
        TRAIN => {
            if config.zerg_building_training {
                let group_flags =
                    (bw::units_dat()[0x2c].data as *mut u8).offset(unit.id().0 as isize);
                let orig_flags = *group_flags;
                *group_flags &= !0x1;
                zerg_group_flags = Some((group_flags, orig_flags));
            }
        }
        SPREAD_CREEP => {
            let game = crate::game::get();
            if let Some(new_timer) = upgrades::creep_spread_time(&config, game, unit) {
                if new_timer == -1 {
                    (**unit).unit_specific[0xc] = 10;
                } else {
                    creep_spread_time = Some(new_timer as u8);
                }
            }
            if let Some(new_timer) = upgrades::larva_spawn_time(&config, game, unit) {
                if new_timer == -1 {
                    (**unit).unit_specific[0xa] = 10;
                } else {
                    larva_spawn_time = Some(new_timer as u8);
                }
            }
            spread_creep_check = (**unit).unit_specific[0xc] == 0;
            larva_spawn_check = (**unit).unit_specific[0xa] == 0;
        }
        SPAWNING_LARVA => {
            let game = crate::game::get();
            if let Some(new_timer) = upgrades::larva_spawn_time(&config, game, unit) {
                if new_timer == -1 {
                    (**unit).unit_specific[0xa] = 10;
                } else {
                    larva_spawn_time = Some(new_timer as u8);
                }
            }
            larva_spawn_check = (**unit).unit_specific[0xa] == 0;
        }
        _ => ()
    }
    orig(u);
    if spread_creep_check {
        if (**unit).unit_specific[0xc] != 0 {
            if let Some(new_timer) = creep_spread_time {
                (**unit).unit_specific[0xc] = new_timer;
            }
        }
    }
    if larva_spawn_check {
        if (**unit).unit_specific[0xa] != 0 {
            if let Some(new_timer) = larva_spawn_time {
                (**unit).unit_specific[0xa] = new_timer;
            }
        }
    }
    if let Some((out, orig)) = zerg_group_flags {
        *out = orig;
    }
}
